package com.example.hotelmanagement.services;

import com.example.hotelmanagement.bindingModels.GuestBindingModel;
import com.example.hotelmanagement.domain.Gender;
import com.example.hotelmanagement.entities.GenderEntity;
import com.example.hotelmanagement.entities.GuestEntity;
import com.example.hotelmanagement.exceptions.InvalidGuestException;
import com.example.hotelmanagement.exceptions.ItemNotFoundException;
import com.example.hotelmanagement.repositories.BookingRepository;
import com.example.hotelmanagement.repositories.GenderRepository;
import com.example.hotelmanagement.repositories.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GuestService {

    private final GuestRepository guestRepository;
    private final GenderRepository genderRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public GuestService(GuestRepository guestRepository, GenderRepository genderRepository, BookingRepository bookingRepository) {
        this.guestRepository = guestRepository;
        this.genderRepository = genderRepository;
        this.bookingRepository = bookingRepository;
    }

    public void createGuest(GuestBindingModel guestBindingModel) {
        validateGuest(guestBindingModel);
        if (existGuestByPassportId(guestBindingModel.getPassportId())) {
            throw new InvalidGuestException("Guest with passport number: " + guestBindingModel.getPassportId() + " already exist!");
        } else {
            saveGuest(new GuestEntity(), guestBindingModel);
        }
    }

    public void updateGuest(int id, GuestBindingModel guestBindingModel) {
        validateGuest(guestBindingModel);
        saveGuest(findGuestById(id), guestBindingModel);
    }

    public GuestBindingModel getGuestById(int id) {
        return convertGuestEntityToGuestBM(findGuestById(id));
    }

    public List<GuestBindingModel> getAllGuests() {
        List<GuestEntity> guests = guestRepository.findAll();
        List<GuestBindingModel> guestList = new ArrayList<>();

        for (GuestEntity guest : guests) {
            guestList.add(convertGuestEntityToGuestBM(guest));
        }

        return Collections.unmodifiableList(guestList);
    }

    @Transactional
    public void deleteGuestById(int id) {
        if(existGuestById(id)) {
            bookingRepository.deleteByGuest(findGuestById(id));
            guestRepository.deleteById(id);
        }
    }

    public GuestEntity findGuestById(int id) {
        GuestEntity guest = guestRepository.findById(id).orElse(null);

        if (guest != null) {
            return guest;
        } else {
            throw new ItemNotFoundException("Guest with id " + id + " not found!");
        }
    }

    private boolean existGuestByPassportId(String passportId) {
        return guestRepository.existsByPassportId(passportId);
    }

    private boolean existGuestById(int id) {
        return guestRepository.existsById(id);
    }

    private void saveGuest(GuestEntity guest, GuestBindingModel guestBindingModel) {
        guest.setFirstName(guestBindingModel.getFirstName());
        guest.setLastName(guestBindingModel.getLastName());
        guest.setAge(guestBindingModel.getAge());
        guest.setEmail(guestBindingModel.getEmail());
        guest.setPassportId(guestBindingModel.getPassportId());
        guest.setPhoneNumber(guestBindingModel.getPhoneNumber());
        guest.setGender(findGender(guestBindingModel.getGender()));

        guestRepository.save(guest);
    }

    private GuestBindingModel convertGuestEntityToGuestBM(GuestEntity guest) {
        GuestBindingModel guestBindingModel = new GuestBindingModel();

        guestBindingModel.setFirstName(guest.getFirstName());
        guestBindingModel.setLastName((guest.getLastName()));
        guestBindingModel.setAge(guest.getAge());
        guestBindingModel.setEmail(guest.getEmail());
        guestBindingModel.setId(guest.getId());
        guestBindingModel.setPassportId(guest.getPassportId());
        guestBindingModel.setPhoneNumber(guest.getPhoneNumber());
        guestBindingModel.setGender(guest.getGender().getName().name());

        return guestBindingModel;
    }

    private void validateGuest(GuestBindingModel guestBindingModel) {
        if (guestBindingModel.getFirstName() == null || guestBindingModel.getFirstName().isEmpty()) {
            throw new InvalidGuestException("First name of the guest can not be empty");
        }
        if (guestBindingModel.getLastName() == null || guestBindingModel.getLastName().isEmpty()) {
            throw new InvalidGuestException("Last name of the guest can not be empty");
        }
        if (guestBindingModel.getPassportId() == null || guestBindingModel.getPassportId().isEmpty()) {
            throw new InvalidGuestException("Passport number of the guest can not be empty");
        }
        if (guestBindingModel.getPhoneNumber() == null || guestBindingModel.getPhoneNumber().isEmpty()) {
            throw new InvalidGuestException("Phone number of the guest can not be empty");
        }
        if (guestBindingModel.getAge() <= 0) {
            throw new InvalidGuestException("Age can not be zero or negative");
        }
        if (guestBindingModel.getGender() == null || findGender(guestBindingModel.getGender()) == null) {
            throw new InvalidGuestException("Gender is undefined");
        }
    }

    private GenderEntity findGender(String name) {
        switch (name.toLowerCase()) {
            case "male":
                return genderRepository.findByName(Gender.MALE);
            case "female":
                return genderRepository.findByName(Gender.FEMALE);

            default:
                return null;
        }
    }
}
