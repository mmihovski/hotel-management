package com.example.hotelmanagement.services;

import com.example.hotelmanagement.bindingModels.BookingBindingModel;
import com.example.hotelmanagement.bindingModels.RoomBindingModel;
import com.example.hotelmanagement.entities.BookingEntity;
import com.example.hotelmanagement.entities.GuestEntity;
import com.example.hotelmanagement.entities.RoomEntity;
import com.example.hotelmanagement.exceptions.InvalidBookingException;
import com.example.hotelmanagement.exceptions.ItemNotFoundException;
import com.example.hotelmanagement.repositories.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private RoomService roomService;
    private GuestService guestService;

    @Autowired
    public BookingService(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Autowired
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }

    @Autowired
    public void setGuestService(GuestService guestService) {
        this.guestService = guestService;
    }

    public BookingBindingModel getBookingById(int id) {
        return convertBookingEntityToBookingBM(findBookingById(id));
    }

    public List<BookingBindingModel> getBookingsByRoomId(int id) {

        List<BookingBindingModel> bookingList = new ArrayList<>();

        RoomEntity room = roomService.findRoomById(id);

        List<BookingEntity> bookings = bookingRepository.findAllByRoom(room);

        for (BookingEntity booking : bookings) {
            bookingList.add(convertBookingEntityToBookingBM(booking));
        }
        return Collections.unmodifiableList(bookingList);
    }

    public List<BookingBindingModel> getAllBookings() {

        List<BookingBindingModel> bookingList = new ArrayList<>();
        List<BookingEntity> bookings = bookingRepository.findAll();

        for (BookingEntity booking : bookings) {
            bookingList.add(convertBookingEntityToBookingBM(booking));
        }

        return Collections.unmodifiableList(bookingList);
    }

    public List<BookingBindingModel> getBookingsByGuestId(int id) {

        List<BookingBindingModel> bookingList = new ArrayList<>();
        GuestEntity guest = guestService.findGuestById(id);
        List<BookingEntity> bookings = bookingRepository.findAllByGuest(guest);

        for (BookingEntity booking : bookings) {
            bookingList.add(convertBookingEntityToBookingBM(booking));
        }

        return Collections.unmodifiableList(bookingList);
    }

    public void createBooking(BookingBindingModel bookingBindingModel) {
        validateBooking(bookingBindingModel);
        checkRoomCapacity(bookingBindingModel);

        saveBooking(new BookingEntity(), bookingBindingModel);
    }

    public void updateBooking(int id, BookingBindingModel bookingBindingModel) {
        existBookingById(id);
        saveBooking(findBookingById(bookingBindingModel.getId()), bookingBindingModel);
    }

    public void deleteById(int id) {
        if (existBookingById(id)) {
            bookingRepository.deleteById(id);
        }
    }

    private void saveBooking(BookingEntity booking, BookingBindingModel bookingBindingModel) {
        GuestEntity guest = guestService.findGuestById(bookingBindingModel.getGuestId());
        RoomEntity room = roomService.findRoomById(bookingBindingModel.getRoomId());

        booking.setGuest(guest);
        booking.setRoom(room);
        booking.setNumberOfPeople(bookingBindingModel.getNumberOfPeople());
        booking.setFrom(LocalDate.parse(bookingBindingModel.getFromDate()));
        booking.setTo(LocalDate.parse(bookingBindingModel.getToDate()));

        bookingRepository.save(booking);
    }

    private BookingEntity findBookingById(int id) {
        BookingEntity booking = bookingRepository.findById(id).orElse(null);

        if (booking != null) {
            return booking;
        } else {
            throw new ItemNotFoundException("Booking with id " + id + " not found!");
        }
    }

    private boolean existBookingById(int id) {
        if (bookingRepository.existsById(id)) {
            return true;
        } else {
            throw new ItemNotFoundException("Booking with id " + id + " not found!");
        }
    }

    private BookingBindingModel convertBookingEntityToBookingBM(BookingEntity booking) {
        BookingBindingModel bookingBindingModel = new BookingBindingModel();

        bookingBindingModel.setId(booking.getId());
        bookingBindingModel.setGuestId(booking.getGuest().getId());
        bookingBindingModel.setRoomId(booking.getRoom().getId());
        bookingBindingModel.setNumberOfPeople(booking.getNumberOfPeople());
        bookingBindingModel.setFromDate(booking.getFrom().toString());
        bookingBindingModel.setToDate(booking.getTo().toString());

        return bookingBindingModel;
    }

    private void validateBooking(BookingBindingModel bookingBindingModel) {
        LocalDate fromDate, toDate;

        checkForNull(bookingBindingModel);

        try{
            fromDate = LocalDate.parse(bookingBindingModel.getFromDate());
            toDate = LocalDate.parse(bookingBindingModel.getToDate());
        }
        catch(DateTimeParseException e){
            throw new InvalidBookingException("Dates format are invalid");
        }
        checkDates(fromDate, toDate);
        checkRoomCapacity(bookingBindingModel);

        if (!isFreeForDateInterval(fromDate, toDate, bookingBindingModel.getRoomId())) {
            throw new InvalidBookingException("Room is not free for this date interval");
        }
    }

    private void checkForNull(BookingBindingModel bookingBindingModel) {
        if (bookingBindingModel.getFromDate() == null || bookingBindingModel.getFromDate().isEmpty()) {
            throw new InvalidBookingException("From date can not be empty");
        }
        if (bookingBindingModel.getToDate() == null || bookingBindingModel.getToDate().isEmpty()) {
            throw new InvalidBookingException("To date can not be empty");
        }
        if (bookingBindingModel.getGuestId() <= 0) {
            throw new InvalidBookingException("Guest is undefined");
        }
        if (bookingBindingModel.getRoomId() <= 0) {
            throw new InvalidBookingException("Room is undefined");
        }
        if (bookingBindingModel.getNumberOfPeople() <= 0) {
            throw new InvalidBookingException("Number of people can not be zero or negative");
        }
    }

    private void checkDates(LocalDate fromDate, LocalDate toDate) {
        if (fromDate.isEqual(toDate) || fromDate.isAfter(toDate)) {
            throw new InvalidBookingException("From date can not be equal or before after date!");
        }
        if (fromDate.isBefore(LocalDate.now()) || toDate.isBefore(LocalDate.now())) {
            throw new InvalidBookingException("The dates can not be before today!");
        }
    }

    private void checkRoomCapacity(BookingBindingModel booking) {
        RoomBindingModel room = roomService.getRoomById(booking.getRoomId());

        if (booking.getNumberOfPeople() > room.getCapacity()) {
            throw new InvalidBookingException("Тhe room does not have enough space");
        }
    }

    private boolean isFreeForDateInterval(LocalDate fromDate, LocalDate toDate, int roomId) {

        RoomEntity room = roomService.findRoomById(roomId);
        List<BookingEntity> bookingsList = bookingRepository.findAllByRoom(room);

        for (BookingEntity booking : bookingsList) {
            if (!(fromDate.isAfter(booking.getTo()) || toDate.isBefore(booking.getFrom())
                    || fromDate.isEqual(booking.getTo()) || toDate.isEqual(booking.getFrom()))) {
                return false;
            }
        }
        return true;
    }
}
