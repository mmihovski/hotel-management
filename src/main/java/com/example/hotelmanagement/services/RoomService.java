package com.example.hotelmanagement.services;

import com.example.hotelmanagement.bindingModels.BedBindingModel;
import com.example.hotelmanagement.bindingModels.RoomBindingModel;
import com.example.hotelmanagement.domain.BedType;
import com.example.hotelmanagement.entities.BedTypeEntity;
import com.example.hotelmanagement.entities.RoomBedEntity;
import com.example.hotelmanagement.entities.RoomEntity;
import com.example.hotelmanagement.exceptions.InvalidRoomException;
import com.example.hotelmanagement.exceptions.ItemNotFoundException;
import com.example.hotelmanagement.repositories.BedTypeRepository;
import com.example.hotelmanagement.repositories.BookingRepository;
import com.example.hotelmanagement.repositories.RoomBedRepository;
import com.example.hotelmanagement.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final BedTypeRepository bedTypeRepository;
    private final RoomBedRepository roomBedRepository;
    private final BookingRepository bookingRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository, BedTypeRepository bedTypeRepository, RoomBedRepository roomBedRepository, BookingRepository bookingRepository) {
        this.roomRepository = roomRepository;
        this.bedTypeRepository = bedTypeRepository;
        this.roomBedRepository = roomBedRepository;
        this.bookingRepository = bookingRepository;
    }

    public void createRooms(RoomBindingModel... rooms) {
        for (RoomBindingModel roomBindingModel : rooms) {
            validateRoom(roomBindingModel);
            if (existsRoomByNumber(roomBindingModel.getNumber())) {
                throw new InvalidRoomException("Room with number " + roomBindingModel.getNumber() + " already exist! " + "Provide correct number");
            } else {
                RoomEntity room = new RoomEntity(roomBindingModel.getNumber());
                List<RoomBedEntity> bedList = new ArrayList<>();

                saveBeds(roomBindingModel, room, bedList);

                roomRepository.save(room);
                roomBedRepository.saveAll(bedList);
            }
        }
    }

    @Transactional
    public void updateRoom(int id, RoomBindingModel roomBindingModel) {
        validateRoom(roomBindingModel);

        if (existsRoomById(id)) {
            roomBedRepository.deleteAllByRoomId(id);
        }

        RoomEntity room = findRoomById(id);

        List<RoomBedEntity> bedList = room.getBeds();
        bedList.clear();

        saveBeds(roomBindingModel, room, bedList);

        roomBedRepository.saveAll(bedList);
        roomRepository.save(room);
    }

    public List<RoomBindingModel> getAllRooms() {
        List<RoomEntity> rooms = roomRepository.findAll();
        List<RoomBindingModel> roomsList = new ArrayList<>();

        for (RoomEntity room : rooms) {
            roomsList.add(convertRoomEntityToRoomBM(room));
        }

        return Collections.unmodifiableList(roomsList);
    }

    public RoomBindingModel getRoomById(int id) {
        return convertRoomEntityToRoomBM(findRoomById(id));
    }

    @Transactional
    public void deleteRoomById(int id) {
        if (existsRoomById(id)) {
            bookingRepository.deleteByRoom(findRoomById(id));
            roomBedRepository.deleteAllByRoomId(id);
            roomRepository.deleteById(id);
        }
    }

    public RoomEntity findRoomById(int id) {
        RoomEntity room = roomRepository.findById(id).orElse(null);

        if (room != null) {
            return room;
        } else {
            throw new ItemNotFoundException("Room with id " + id + " not found!");
        }
    }

    private boolean existsRoomById(int id) {
        if (roomRepository.existsById(id)) {
            return true;
        } else {
            throw new ItemNotFoundException("Room with id " + id + " not found!");
        }
    }

    private boolean existsRoomByNumber(int number) {
        return roomRepository.existsByNumber(number);
    }

    private RoomBindingModel convertRoomEntityToRoomBM(RoomEntity room) {
        RoomBindingModel roomBindingModel = new RoomBindingModel();
        List<BedBindingModel> bedList = new ArrayList<>();

        roomBindingModel.setId(room.getId());
        roomBindingModel.setNumber(room.getNumber());
        roomBindingModel.setCapacity(room.getCapacity());

        for (RoomBedEntity roomBed : room.getBeds()) {
            BedBindingModel bedBindingModel = new BedBindingModel();
            bedBindingModel.setBedType(roomBed.getBedType().getType().name().toLowerCase());
            bedBindingModel.setCount(roomBed.getCount());
            bedList.add(bedBindingModel);
        }

        roomBindingModel.setBeds(bedList);

        return roomBindingModel;
    }

    private void saveBeds(RoomBindingModel roomBindingModel, RoomEntity room, List<RoomBedEntity> bedList) {
        for (BedBindingModel bed : roomBindingModel.getBeds()) {

            BedTypeEntity bedType = bedTypeRepository.findByType(findBedType(bed.getBedType()));
            RoomBedEntity roomBeds = new RoomBedEntity(room, bedType, bed.getCount());

            bedList.add(roomBeds);
        }

        room.setBeds(bedList);
    }

    private void validateRoom(RoomBindingModel roomBindingModel) {
        if (roomBindingModel.getNumber() <= 0) {
            throw new InvalidRoomException("Room number can not be zero or negative");
        }

        List<BedBindingModel> bedList = roomBindingModel.getBeds();

        for (BedBindingModel bed : bedList) {
            if (bed.getBedType() == null || findBedType(bed.getBedType()) == null) {
                throw new InvalidRoomException("The bed type is undefined");
            }
            if (bed.getCount() <= 0) {
                throw new InvalidRoomException("Bed count can not be zero or negative");
            }
        }
    }

    private BedType findBedType(String name) {
        switch (name.toLowerCase()) {
            case "single":
                return BedType.SINGLE;
            case "double":
                return BedType.DOUBLE;
            case "king size":
                return BedType.KING_SIZE;
            default:
                return null;
        }
    }
}
