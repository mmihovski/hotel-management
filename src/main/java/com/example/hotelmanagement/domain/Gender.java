package com.example.hotelmanagement.domain;

public enum Gender {
    MALE, FEMALE
}
