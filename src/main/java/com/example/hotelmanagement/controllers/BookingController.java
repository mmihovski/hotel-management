package com.example.hotelmanagement.controllers;

import com.example.hotelmanagement.bindingModels.BookingBindingModel;
import com.example.hotelmanagement.bindingModels.RoomBindingModel;
import com.example.hotelmanagement.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bookings", produces = MediaType.APPLICATION_JSON_VALUE)
public class BookingController {

    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping
    List<BookingBindingModel> getAllBookings() {
        return bookingService.getAllBookings();
    }

    @GetMapping("/{id}")
    BookingBindingModel getBookingById(@PathVariable int id) {
        return bookingService.getBookingById(id);
    }

    @PutMapping("/{id}")
    void updateBookingById(@PathVariable int id, @RequestBody BookingBindingModel bookingBindingModel) {
        bookingService.updateBooking(id, bookingBindingModel);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void addBooking(@RequestBody BookingBindingModel booking) {
        bookingService.createBooking(booking);
    }

    @DeleteMapping("/{id}")
    void deleteBookingById(@PathVariable int id){
        bookingService.deleteById(id);
    }
}
