package com.example.hotelmanagement.controllers;

import com.example.hotelmanagement.bindingModels.BookingBindingModel;
import com.example.hotelmanagement.bindingModels.RoomBindingModel;
import com.example.hotelmanagement.services.BookingService;
import com.example.hotelmanagement.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rooms", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {

    private final RoomService roomService;
    private final BookingService bookingService;

    @Autowired
    public RoomController(RoomService roomService, BookingService bookingService) {
        this.roomService = roomService;
        this.bookingService = bookingService;
    }

    @GetMapping
    List<RoomBindingModel> getAllRooms() {
        return roomService.getAllRooms();
    }

    @GetMapping("/{id}")
    RoomBindingModel getRoomById(@PathVariable int id) {
        return roomService.getRoomById(id);
    }

    @GetMapping("/{id}/bookings")
    List<BookingBindingModel> getBookingsByRoomId(@PathVariable int id) {
        return bookingService.getBookingsByRoomId(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void addNewRoom(@RequestBody RoomBindingModel... room) {
        roomService.createRooms(room);
    }

    @PutMapping("/{id}")
    void updateRoomById(@PathVariable int id, @RequestBody RoomBindingModel room) {
        roomService.updateRoom(id, room);
    }

    @DeleteMapping("/{id}")
    void deleteRoomById(@PathVariable int id) {
        roomService.deleteRoomById(id);
    }
}
