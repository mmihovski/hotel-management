package com.example.hotelmanagement.controllers;


import com.example.hotelmanagement.bindingModels.BookingBindingModel;
import com.example.hotelmanagement.bindingModels.GuestBindingModel;
import com.example.hotelmanagement.services.BookingService;
import com.example.hotelmanagement.services.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/guests", produces = MediaType.APPLICATION_JSON_VALUE)
public class GuestController {

    private final GuestService guestService;
    private final BookingService bookingService;

    @Autowired
    public GuestController(GuestService guestService, BookingService bookingService) {
        this.guestService = guestService;
        this.bookingService = bookingService;
    }

    @GetMapping
    List<GuestBindingModel> getAllGuests(){
        return guestService.getAllGuests();
    }

    @GetMapping("/{id}")
    GuestBindingModel getGuestById(@PathVariable int id) {
        return guestService.getGuestById(id);
    }

    @GetMapping("/{id}/bookings")
    List<BookingBindingModel> getBookingByGuestId(@PathVariable int id) {
        return bookingService.getBookingsByGuestId(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    void addGuest(@RequestBody GuestBindingModel guest) {
        guestService.createGuest(guest);
    }

    @PutMapping("/{id}")
    void updateGuest(@PathVariable int id, @RequestBody GuestBindingModel guest){
        guestService.updateGuest(id, guest);
    }

    @DeleteMapping("/{id}")
    void deleteGuestById(@PathVariable int id){
        guestService.deleteGuestById(id);
    }
}
