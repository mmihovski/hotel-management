package com.example.hotelmanagement.bindingModels;

import java.util.ArrayList;
import java.util.List;

public class RoomBindingModel {

    private int id;
    private int number;
    private int capacity;
    private List<BedBindingModel> beds;

    public RoomBindingModel() {
        this.beds = new ArrayList<>();
    }

    public RoomBindingModel(int id, int number, int capacity, List<BedBindingModel> beds) {
        this.id = id;
        this.number = number;
        this.capacity = capacity;
        this.beds = beds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<BedBindingModel> getBeds() {
        return beds;
    }

    public void setBeds(List<BedBindingModel> beds) {
        this.beds = beds;
    }
}
