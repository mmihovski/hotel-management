package com.example.hotelmanagement.bindingModels;

public class BedBindingModel {

    private String bedType;
    private int count;

    public BedBindingModel() {
    }

    public BedBindingModel(String bedType, int count) {
        this.bedType = bedType;
        this.count = count;
    }

    public String getBedType() {
        return bedType;
    }

    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
