package com.example.hotelmanagement.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RoomBedId implements Serializable {

    @Column(name = "room_id")
    private int roomId;

    @Column(name = "bed_id")
    private int bedId;

    private RoomBedId() {
    }

    public RoomBedId(int roomId, int bedId) {
        this.roomId = roomId;
        this.bedId = bedId;
    }

    public int getRoomId() {
        return roomId;
    }

    public int getBedId() {
        return bedId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        RoomBedId that = (RoomBedId) o;
        return Objects.equals(roomId, that.roomId) &&
                Objects.equals(bedId, that.bedId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId, bedId);
    }
}
