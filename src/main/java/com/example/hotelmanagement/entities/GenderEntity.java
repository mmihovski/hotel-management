package com.example.hotelmanagement.entities;

import com.example.hotelmanagement.domain.Gender;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "genders")
public class GenderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private int id;

    @Column(name = "name", nullable = false, updatable = false, unique = true)
    @Enumerated(EnumType.STRING)
    private Gender name;

    @OneToMany(targetEntity = GuestEntity.class, mappedBy = "gender")
    private Set<GenderEntity> guests;

    public GenderEntity() {
        guests = new HashSet<>();
    }

    public GenderEntity(Gender name) {
        guests = new HashSet<>();
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gender getName() {
        return name;
    }

    public void setName(Gender name) {
        this.name = name;
    }

    public Set<GenderEntity> getGuests() {
        return guests;
    }

    public void setGuests(Set<GenderEntity> guests) {
        this.guests = guests;
    }
}
