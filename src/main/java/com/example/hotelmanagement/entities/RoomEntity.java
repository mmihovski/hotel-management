package com.example.hotelmanagement.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "rooms")
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private int id;

    @Column(name = "number", nullable = false, updatable = false, unique = true)
    private int number;

    @Column(name = "capacity", nullable = false)
    private int capacity;

    /*
    @ManyToMany(targetEntity = BedTypeEntity.class, fetch = FetchType.EAGER)
    @JoinTable(name = "beds_of_rooms",
            joinColumns = @JoinColumn(name = "room_id"), inverseJoinColumns = @JoinColumn(name = "bed_type_id"))
    private List<BedTypeEntity> beds;
*/
    @OneToMany(targetEntity = BookingEntity.class, mappedBy = "room")
    private Set<BookingEntity> bookings;

    @OneToMany(targetEntity = RoomBedEntity.class, mappedBy = "room")
    @JsonManagedReference
    private List<RoomBedEntity> beds;

    public RoomEntity() {
        beds = new ArrayList<>();
        bookings = new HashSet<>();
    }

    public RoomEntity(int number) {
        this.bookings = new HashSet<>();
        this.beds = new ArrayList<>();
        this.number = number;
    }

    private void roomCapacitySetter() {
        capacity = 0;

        for (RoomBedEntity bed : beds) {
            capacity += bed.getBedType().getSize() * bed.getCount();
        }
        /*
        if (capacity == 0) {
            throw new FailedInitializationException("Room can not be empty");
        }*/
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCapacity() {
        return capacity;
    }

    public List<RoomBedEntity> getBeds() {
        return beds;
    }

    public void setBeds(RoomBedEntity... beds) {
        this.beds = Arrays.asList(beds);
        roomCapacitySetter();
    }

    public void setBeds(List<RoomBedEntity> beds) {
        this.beds = beds;
        roomCapacitySetter();
    }

    public Set<BookingEntity> getBookings() {
        return bookings;
    }

    public void setBookings(Set<BookingEntity> bookings) {
        this.bookings = bookings;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        RoomEntity that = (RoomEntity) obj;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
