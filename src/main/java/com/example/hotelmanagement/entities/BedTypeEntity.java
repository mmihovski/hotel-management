package com.example.hotelmanagement.entities;

import com.example.hotelmanagement.domain.BedType;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bed_types")
public class BedTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private int id;

    @Column(name = "type", nullable = false, updatable = false, unique = true)
    @Enumerated(EnumType.STRING)
    private BedType type;

    @Column(name = "size", nullable = false, updatable = false)
    private int size;

    @OneToMany(targetEntity = RoomBedEntity.class, mappedBy = "bedType", orphanRemoval = true)
    @JsonManagedReference
    private List<RoomBedEntity> rooms;

    public BedTypeEntity() {

    }

    public BedTypeEntity(BedType name) {
        this.type = name;
        this.size = name.getSize();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BedType getType() {
        return type;
    }

    public void setType(BedType type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<RoomBedEntity> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomBedEntity> rooms) {
        this.rooms = rooms;
    }
}
