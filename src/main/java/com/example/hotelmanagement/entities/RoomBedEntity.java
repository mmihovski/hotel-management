package com.example.hotelmanagement.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "room_beds")
public class RoomBedEntity {

    @EmbeddedId
    private RoomBedId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("roomId")
    @JsonBackReference
    private RoomEntity room;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("bedId")
    @JsonBackReference
    private BedTypeEntity bedType;

    @Column(name = "count")
    private int count;

    private RoomBedEntity() {
    }

    public RoomBedEntity(RoomEntity room, BedTypeEntity bedType, int count) {
        this.room = room;
        this.bedType = bedType;
        this.count = count;
        this.id = new RoomBedId(room.getId(), bedType.getId());
    }

    public RoomBedId getId() {
        return id;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public BedTypeEntity getBedType() {
        return bedType;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        RoomBedEntity that = (RoomBedEntity) obj;
        return Objects.equals(room, that.room) && Objects.equals(bedType, that.bedType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(room, bedType);
    }
}
