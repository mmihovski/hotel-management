package com.example.hotelmanagement.databaseService;

import com.example.hotelmanagement.domain.Gender;
import com.example.hotelmanagement.entities.GenderEntity;
import com.example.hotelmanagement.repositories.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class GenderService {

    private final GenderRepository genderRepository;

    @Autowired
    public GenderService(GenderRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    @PostConstruct
    public void seedGendersInDb() {
        if (genderRepository.count() == 0) {
            GenderEntity male = new GenderEntity(Gender.MALE);
            genderRepository.save(male);

            GenderEntity female = new GenderEntity(Gender.FEMALE);
            genderRepository.save(female);
        }
    }
}
