package com.example.hotelmanagement.databaseService;

import com.example.hotelmanagement.domain.BedType;
import com.example.hotelmanagement.entities.BedTypeEntity;
import com.example.hotelmanagement.entities.RoomBedEntity;
import com.example.hotelmanagement.entities.RoomEntity;
import com.example.hotelmanagement.repositories.BedTypeRepository;
import com.example.hotelmanagement.repositories.RoomBedRepository;
import com.example.hotelmanagement.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class RoomDatabaseService {

    private final RoomRepository roomRepository;
    private final BedTypeRepository bedTypeRepository;
    private final RoomBedRepository roomBedRepository;

    @Autowired
    public RoomDatabaseService(RoomRepository roomRepository, BedTypeRepository bedTypeRepository, RoomBedRepository roomBedRepository) {
        this.roomRepository = roomRepository;
        this.bedTypeRepository = bedTypeRepository;
        this.roomBedRepository = roomBedRepository;
    }

    @PostConstruct
    public void seedRoomsInDb() {
        if (roomRepository.count() == 0) {

            BedTypeEntity singleBed = bedTypeRepository.findByType(BedType.SINGLE);
            BedTypeEntity doubleBed = bedTypeRepository.findByType(BedType.DOUBLE);
            BedTypeEntity kingBed = bedTypeRepository.findByType(BedType.KING_SIZE);

            //room 101
            RoomEntity room101 = new RoomEntity(101);
            RoomBedEntity room101beds = new RoomBedEntity(room101, singleBed, 1);
            room101.setBeds(room101beds);
            roomRepository.save(room101);
            roomBedRepository.save(room101beds);

            //room 102
            RoomEntity room102 = new RoomEntity(102);
            RoomBedEntity room102beds = new RoomBedEntity(room102, singleBed, 1);
            room102.setBeds(room102beds);
            roomRepository.save(room102);
            roomBedRepository.save(room102beds);

            //room 103
            RoomEntity room103 = new RoomEntity(103);
            RoomBedEntity room103beds = new RoomBedEntity(room103, doubleBed, 1);
            room103.setBeds(room103beds);
            roomRepository.save(room103);
            roomBedRepository.save(room103beds);

            //room 201
            RoomEntity room201 = new RoomEntity(201);
            RoomBedEntity room201beds = new RoomBedEntity(room201, singleBed, 2);
            room201.setBeds(room201beds);
            roomRepository.save(room201);
            roomBedRepository.save(room201beds);

            //room 202
            RoomEntity room202 = new RoomEntity(202);
            RoomBedEntity room202beds = new RoomBedEntity(room202, singleBed, 3);
            room202.setBeds(room202beds);
            roomRepository.save(room202);
            roomBedRepository.save(room202beds);

            //room 203
            RoomEntity room203 = new RoomEntity(203);
            RoomBedEntity room203bed = new RoomBedEntity(room203, doubleBed, 1);
            RoomBedEntity room203beds = new RoomBedEntity(room203, singleBed, 1);
            room203.setBeds(room203bed, room203beds);
            roomRepository.save(room203);
            roomBedRepository.save(room203bed);
            roomBedRepository.save(room203beds);

            //room 301
            RoomEntity room301 = new RoomEntity(301);
            RoomBedEntity room301bed = new RoomBedEntity(room301, kingBed, 1);
            room301.setBeds(room301bed);
            roomRepository.save(room301);
            roomBedRepository.save(room301bed);

            //room 302
            RoomEntity room302 = new RoomEntity(302);
            RoomBedEntity room302bed = new RoomBedEntity(room302, kingBed, 1);
            room302.setBeds(room302bed);
            roomRepository.save(room302);
            roomBedRepository.save(room302bed);

            //room 303
            RoomEntity room303 = new RoomEntity(303);
            RoomBedEntity room303beds = new RoomBedEntity(room303, singleBed, 2);
            RoomBedEntity room303bed = new RoomBedEntity(room303, kingBed, 1);
            room303.setBeds(room303beds, room303bed);
            roomRepository.save(room303);
            roomBedRepository.save(room303beds);
            roomBedRepository.save(room303bed);
        }
    }
}
