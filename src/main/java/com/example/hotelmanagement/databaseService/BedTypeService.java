package com.example.hotelmanagement.databaseService;

import com.example.hotelmanagement.domain.BedType;
import com.example.hotelmanagement.entities.BedTypeEntity;
import com.example.hotelmanagement.repositories.BedTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class BedTypeService {

    private final BedTypeRepository bedTypeRepository;

    @Autowired
    public BedTypeService(BedTypeRepository bedTypeRepository) {
        this.bedTypeRepository = bedTypeRepository;
    }

    @PostConstruct
    public void seedGendersInDb() {
        if (bedTypeRepository.count() == 0) {

            BedTypeEntity singleBed = new BedTypeEntity(BedType.SINGLE);
            bedTypeRepository.save(singleBed);

            BedTypeEntity doubleBed = new BedTypeEntity(BedType.DOUBLE);
            bedTypeRepository.save(doubleBed);

            BedTypeEntity kingBed = new BedTypeEntity(BedType.KING_SIZE);
            bedTypeRepository.save(kingBed);

        }
    }
}
