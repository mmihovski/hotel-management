package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.entities.GuestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestRepository extends JpaRepository<GuestEntity, Integer> {
    boolean existsByPassportId(String passportId);
}
