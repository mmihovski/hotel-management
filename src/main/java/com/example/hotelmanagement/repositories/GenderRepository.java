package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.domain.Gender;
import com.example.hotelmanagement.entities.GenderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<GenderEntity, Integer> {
    GenderEntity findByName(Gender gender);
}
