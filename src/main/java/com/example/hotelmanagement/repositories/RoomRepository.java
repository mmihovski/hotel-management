package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.entities.BookingEntity;
import com.example.hotelmanagement.entities.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
    RoomEntity findByNumber(int number);
    boolean existsByNumber(int number);
}
