package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.domain.BedType;
import com.example.hotelmanagement.entities.BedTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BedTypeRepository extends JpaRepository<BedTypeEntity, Integer> {
    BedTypeEntity findByType(BedType type);
}
