package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.entities.RoomBedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomBedRepository extends JpaRepository<RoomBedEntity, Integer> {
    List<RoomBedEntity> findAllByRoomId(int id);
    void deleteAllByRoomId(int id);
}
