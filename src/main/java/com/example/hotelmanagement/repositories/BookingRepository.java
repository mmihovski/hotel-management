package com.example.hotelmanagement.repositories;

import com.example.hotelmanagement.entities.BookingEntity;
import com.example.hotelmanagement.entities.GuestEntity;
import com.example.hotelmanagement.entities.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {
    List<BookingEntity> findAllByRoom(RoomEntity room);

    List<BookingEntity> findAllByGuest(GuestEntity guest);

    void deleteByRoom(RoomEntity room);

    void deleteByGuest(GuestEntity guest);
}
