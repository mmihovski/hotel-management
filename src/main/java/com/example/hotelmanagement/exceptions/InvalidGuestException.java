package com.example.hotelmanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidGuestException extends RuntimeException {

    public InvalidGuestException() {
        super();
    }

    public InvalidGuestException(String message) {
        super(message);
    }

    public InvalidGuestException(String message, Throwable cause) {
        super(message, cause);
    }
}
